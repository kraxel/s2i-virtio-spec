#!/bin/sh

css="$1"
if test ! -f "$css"; then
	echo "usage: $0 <css-file>"
	exit 1
fi

orig="${css%.css}-orig.css"
if test ! -f "$orig"; then
	cp "$css" "$orig"
fi

(
	echo "@import url(darkmode.css);"
	sed	-e 's/black/var(--color-black)/g' \
		-e 's/#F5F5F5/var(--color-white)/g' \
		-e 's/#552681/var(--color-blue)/g' \
		< "$orig"
) > "$css"
