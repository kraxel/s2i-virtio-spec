FROM registry.gitlab.com/kraxel/s2i-base-httpd

ENV SUMMARY="virtio spec" \
    DESCRIPTION="Platform for building virtio specification (centos8)"

LABEL maintainer="Gerd Hoffmann <kraxel@redhat.com>" \
      summary="${SUMMARY}" \
      description="${DESCRIPTION}" \
      io.k8s.display-name="${SUMMARY}" \
      io.k8s.description="${DESCRIPTION}" \
      io.openshift.tags="virtio"

USER root

RUN source /etc/profile.d/proxy.sh; \
    dnf update -y && \
    dnf install -y liberation-fonts \
                   texlive-amsfonts \
                   texlive-cm \
                   texlive-courier \
                   texlive-latex-fonts \
                   texlive-metafont \
                   texlive-metapost \
                   texlive-xetexfontinfo \
                   texlive-zapfding \
		   \
                   texlive-tetex \
                   texlive-tex4ht \
                   /usr/bin/htlatex \
                   /usr/bin/pdflatex \
                   /usr/bin/xelatex \
		   \
		   'tex(ae.sty)' \
		   'tex(aecompl.sty)' \
		   'tex(array.sty)' \
		   'tex(babel.sty)' \
		   'tex(chngcntr.sty)' \
		   'tex(courier.sty)' \
		   'tex(english.ldf)' \
		   'tex(etoolbox.sty)' \
		   'tex(fancyhdr.sty)' \
		   'tex(fancyvrb.sty)' \
		   'tex(fontenc.sty)' \
		   'tex(fontspec.sty)' \
		   'tex(framed.sty)' \
		   'tex(geometry.sty)' \
		   'tex(graphicx.sty)' \
		   'tex(hyperref.sty)' \
		   'tex(inputenc.sty)' \
		   'tex(lastpage.sty)' \
		   'tex(lineno.sty)' \
		   'tex(listings.sty)' \
		   'tex(longtable.sty)' \
		   'tex(mdwlist.sty)' \
		   'tex(multirow.sty)' \
		   'tex(parskip.sty)' \
		   'tex(placeins.sty)' \
		   'tex(rotating.sty)' \
		   'tex(setspace.sty)' \
		   'tex(tabularx.sty)' \
		   'tex(titlesec.sty)' \
		   'tex(underscore.sty)' \
		   'tex(url.sty)' \
		   'tex(utf8x.def)' \
		   'tex(xcolor.sty)' \
		   'tex(xifthen.sty)' \
		   'tex(xltxtra.sty)' \
		   'tex(xstring.sty)' \
		   \
                   zip git file && \
    dnf clean all -y

COPY ./s2i/bin/ /usr/libexec/s2i
COPY ./etc/*.conf /etc/httpd/conf.d/

USER 1001

CMD ["/usr/libexec/s2i/usage"]
