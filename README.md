# s2i builder for virtio specification

Source: [https://gitlab.com/kraxel/s2i-virtio-spec](https://gitlab.com/kraxel/s2i-virtio-spec)

Image: registry.gitlab.com/kraxel/s2i-virtio-spec

## Deploy in openshift

```
oc new-app registry.gitlab.com/kraxel/s2i-virtio-spec~https://github.com/oasis-tcs/virtio-spec
```
